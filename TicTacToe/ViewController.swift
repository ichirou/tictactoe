//
//  ViewController.swift
//  TicTacToe
//
//  Created by Richmond Ko on 01/09/2016.
//  Copyright © 2016 Richmond Ko. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    // 1 is nought, 2 is crosses

    @IBOutlet var winnerLabel: UILabel!
    @IBOutlet var playAgainButton: UIButton!
    
    let tapSoundPath = Bundle.main.path(forResource: "write", ofType: "mp3")
    var audioPlayer = AVAudioPlayer()
    
    var activePlayer = 1
    var activeGame = true
    var winnerPlayer = 0
    var gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0] // 0 - empty, 1 - noughts, 2 - crosses
    
    let winningCombinations = [[0, 1, 2], [3, 4, 5], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]
    
    func checkWinner() -> Bool {
        var hasWinner = false
        
        for combination in winningCombinations {
            
            if gameState[combination[0]] != 0 && gameState[combination[0]] == gameState[combination[1]] &&
                gameState[combination[1]] == gameState[combination[2]] {
                //we have a winner
                winnerPlayer = gameState[combination[0]]
                hasWinner = true
            }
        }

        return hasWinner
    }
    
    func showWinner() {
        self.activeGame = false
        winnerLabel.isHidden = false
        playAgainButton.isHidden = false
        UIView.animate(withDuration: 1, animations: {
            self.winnerLabel.center = CGPoint(x: self.winnerLabel.center.x + 500, y: self.winnerLabel.center.y)
            self.playAgainButton.center = CGPoint(x: self.playAgainButton.center.x + 500, y: self.playAgainButton.center.y)
        })
    }
    
    func resetGame() {
        activeGame = true
        activePlayer = 1
        winnerPlayer = 0
        gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        winnerLabel.isHidden = true
        playAgainButton.isHidden = true
        winnerLabel.center = CGPoint(x: winnerLabel.center.x - 500, y: winnerLabel.center.y)
        playAgainButton.center = CGPoint(x: playAgainButton.center.x - 500, y: playAgainButton.center.y)
        
        for i in 1..<10 {
            if let button = view.viewWithTag(i) as? UIButton {
                button.setImage(nil, for: [])
            }
        }

    }
    
    @IBAction func resetGame(_ sender: AnyObject) {
        resetGame()
    }
    
    @IBAction func playAgain(_ sender: AnyObject) {
        resetGame()
    }
    
    @IBAction func buttonPressed(_ sender: AnyObject) {
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: URL(fileURLWithPath: tapSoundPath!))
            
            if activeGame {
                audioPlayer.play()
            }
        }
        catch {
            print("Error in audio AV")
        }
        
        let activePosition = sender.tag - 1
        
        if gameState[activePosition] == 0 && activeGame {
        
            gameState[activePosition] = activePlayer
            
            if activePlayer == 1 {
                sender.setImage(UIImage(named: "nought.png"), for: [])
                activePlayer = 2
            }
            else {
                sender.setImage(UIImage(named: "cross.png"), for: [])
                activePlayer = 1
            }
            
            if checkWinner() {
                if winnerPlayer == 1 {
                    winnerLabel.text = "Noughts have won!"
                }
                else {
                    winnerLabel.text = "Crosses have won!"
                }
                
                showWinner()
            }
            else if gameState[0] != 0 && gameState[1] != 0 && gameState[2] != 0 &&
                gameState[3] != 0 && gameState[4] != 0 && gameState[5] != 0 &&
                gameState[6] != 0 && gameState[7] != 0 && gameState[8] != 0 {
                
                winnerLabel.text = "Nobody won!"
                showWinner()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        winnerLabel.isHidden = true
        playAgainButton.isHidden = true
        
        winnerLabel.layer.borderColor = UIColor.black.cgColor
        winnerLabel.layer.borderWidth = 1
        playAgainButton.layer.borderColor = UIColor.black.cgColor
        playAgainButton.layer.borderWidth = 1
        
        winnerLabel.center = CGPoint(x: winnerLabel.center.x - 500, y: winnerLabel.center.y)
        playAgainButton.center = CGPoint(x: playAgainButton.center.x - 500, y: playAgainButton.center.y)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

